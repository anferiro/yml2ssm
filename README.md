<!--
*** Thanks for checking out this README Template. If you have a suggestion that would
*** make this better please fork the repo and create a pull request or simple open
*** an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->
 
<!-- PROJECT SHIELDS -->

[![Contributors][contributors-shield]](https://github.com/avaldigitallabs/bocc-cc-authentication-service/graphs/contributors)
 
 
<!-- TABLE OF CONTENTS -->
## Table of Contents
 
- [Table of Contents](#table-of-contents)
- [About The Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Local Installation](#local-installation)
- [Usage](#usage)
 
 
 
<!-- ABOUT THE PROJECT -->
## About The Project
 
This project will contain the logic to grap yaml configuration files from a S3 bucket and upload that information into the parameter store.

The bucket name should follow the folowing format

```bash
<cloud>-<projectAcronym>-config-server 

i.e 
bb-cc-config-server
```

In order to setup the folder into the bucket properly you should create the following folders. 

```bash
bb-cc-config-server (bucket)
├── component
│   ├── environment (dev/stg/prod)
│   │   ├── config.yml
```

The properties will be loaded into ssm with the following name format

```bash
/<cloud>-<environment>/<component>/property-name

i.e
/bb-dev/auth/token.expiration
```

The property name will be loaded with point by default but you can change the separator including in the first line of the yml the folowing property

```yaml
separator: '/'
```

### Built With
* [npm](https://npmjs.com)
* [Serverless framework](https://serverless.com/)
 
 
<!-- GETTING STARTED -->
## Getting Started
In order to setup your local environment please follow the steps described below

 
### Prerequisites
You must have installed 
- Node JS 8.10
- npm 5.6.0 
- AWS CLI
- Serverless framework
 
### Local Installation
 
1. Clone the repository
```sh
git clone XXXXXXX
```
2. Compile and Build
```sh
npm install
```
3. deploy to dev environment
```sh
sls deploy -v --stage dev
```
 
<!-- USAGE EXAMPLES -->
## Usage
 
*Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.*
 
<!-- MARKDOWN LINKS & IMAGES -->
[contributors-shield]: https://img.shields.io/badge/contributors-1-orange.svg?style=flat-square
