const ssmManager = require('../../ssm-manager/ssm-manager');
const remover = require('../handler');

jest.mock('../../ssm-manager/ssm-manager.js')

test('it should delete a configuration',()=>{
    ssmManager.deleteConfiguration.mockImplementation((value, callback) => {
        console.log(value)
        expect(value).toMatchObject({
            cloudName: "bb",
            projectName: "cc",
            environment: "dev",
            service: "component"
        })
       callback(undefined,["p1.v1","p2.v2"]);
    })
    let event = {
        Records: [{
            s3: {

                object: {
                    key: "component/dev/config.yml"
                },
                bucket: {
                    name: "bb-cc-config-server"
                }
            }
        }]
    };
    // executions
    remover.ssmRemover(event, undefined, (error) => {
        console.log(error);
        expect(error).toBeUndefined();
    })

    // assertions
    expect(ssmManager.deleteConfiguration).toHaveBeenCalledTimes(1);
})


test('it should fail by names',()=>{
    ssmManager.deleteConfiguration.mockImplementation((value, callback) => {
        console.log(value)
        expect(value).toMatchObject({
            cloudName: "bb",
            projectName: "cc",
            environment: "dev",
            service: "component"
        })
       callback(undefined,["p1.v1","p2.v2"]);
    })
    let event = {
        Records: [{
            s3: {

                object: {
                    key: "component/dev/config.yml"
                },
                bucket: {
                    name: "bb-config-server"
                }
            }
        }]
    };
    // executions
    remover.ssmRemover(event, undefined, (error) => {
        console.log(error);
        expect(error).toBeDefined();
    })

    // assertions
    
})

test('it should fail deleting a configuration',()=>{
    ssmManager.deleteConfiguration.mockImplementation((value, callback) => {
        console.log(value)
        expect(value).toMatchObject({
            cloudName: "bb",
            projectName: "cc",
            environment: "dev",
            service: "component"
        })
       callback("error deleting a configuration");
    })
    let event = {
        Records: [{
            s3: {

                object: {
                    key: "component/dev/config.yml"
                },
                bucket: {
                    name: "bb-cc-config-server"
                }
            }
        }]
    };
    // executions
    remover.ssmRemover(event, undefined, (error) => {
        console.log(error);
        expect(error).toBeDefined();
    })

    // assertions
    expect(ssmManager.deleteConfiguration).toHaveBeenCalledTimes(1);
})

afterEach(() => {
    ssmManager.deleteConfiguration.mockClear();
  });