'use strict';
const ssmManager = require('../ssm-manager/ssm-manager')
const names = require('../utils/names');
module.exports.ssmRemover = (event, context, callback) => {
    console.log("An element has been removed", JSON.stringify(event, undefined, 2));
    event.Records.forEach(record => {
        const key = record.s3.object.key;
        const bucketName = record.s3.bucket.name;
        let configRequest = {};
        names.getNames(key, bucketName, (error, data) => {
            if (error) {
                console.log(error);
                callback(error);
            } else {
                ssmManager.deleteConfiguration(data, (error, data) => {
                    if (error) {
                        console.log(error);
                        callback(error);
                    } else {
                        console.log('the following parameters have been deleted');
                        callback(undefined, data);
                    }
                })
            }
        })
        
            
        
    })
}