const names = require('../names');

test('it should return a configuration request', () => {
    //setup 
    let filePath = 'component/dev/config.yml'
    let bucketName = 'bb-cc-config-server'
    //execution
    let result;
    names.getNames(filePath, bucketName, (error, data) => {
        if (error) {
            console.log(error);
        } else {
            result = data;
        }
    })
    //assertions
    expect(result).toBeDefined();
    expect(result).toMatchObject({
        cloudName: "bb",
        projectName: "cc",
        environment: "dev",
        service: "component",
        fileName: "config.yml"
    });
});


test('it should fail bc the filepath is wrong', () => {
    //setup 
    let filePath = "dev/config.yml"
    let bucketName = "bb-cc-config-server"
    //execution
    names.getNames(filePath, bucketName, (error, data) => {
        if (error) {
            console.log(error);
            expect(error).toBeDefined();
        } else {
            console.log(data);
            fail('No data expected');
        }

    })
    //assertions
    

});

test('it should fail bc the bucketname is wrong', () => {
    //setup 
    let filePath = "componet/dev/config.yml"
    let bucketName = "bb-config-server"
    //execution
    names.getNames(filePath, bucketName, (error, data) => {
        if (error) {
            console.log(error);
            expect(error).toBeDefined();
        } else {
            console.log(data);
            fail('No data expected');
        }

    })
    //assertions
    

});