'use strict';

const path = require('path');


module.exports.getNames = (key, bucketName, callback) => {
    const bucketTemplate = /([^-\n]*)-([^-\n]*)-config-server/g;
    console.log(key);
    console.log(bucketTemplate);
    console.log(bucketName);
    const match = bucketTemplate.exec(bucketName);
    console.log(match);
    let errors = [];
    let configurationRequest = {};
    if (match === null || match.length != 3) {
        errors.push('The bucket name is not compliant with the template <cloud>-<project>-config-server');
    } else {
        configurationRequest.cloudName = match[1];
        configurationRequest.projectName = match[2];
    }
    const fileNameSplited = key.split('/');
    if (fileNameSplited.length != 3) {
        console.log('The folder structure is not correct...');
        console.log('bucketName');
        console.log('├── component');
        console.log('│   ├── environment');
        console.log('│   │   ├── component-config.yml');
        errors.push('The folder structure is not correct...');
    } else {
        configurationRequest.service = key.split('/')[0];
        configurationRequest.environment = key.split('/')[1];
        configurationRequest.fileName = path.basename(key);
    }
    if (errors.length === 0) {
        callback(undefined, configurationRequest);
    } else {
        callback(errors, undefined);
    }
}


