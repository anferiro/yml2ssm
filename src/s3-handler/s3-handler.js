let AWS = require('aws-sdk');


downloadFile = (s3Object) => {
    let s3 = new AWS.S3();
    return s3.getObject(s3Object).promise();
}
module.exports.downloadFile = downloadFile;