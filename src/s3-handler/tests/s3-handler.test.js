var AWS = require('aws-sdk-mock');
const sinon = require('sinon');
const s3Handler = require('../s3-handler');

test('it should return and object', () => {
    // setup
    let getObjectPromise = sinon.stub();
    getObjectPromise.returns({
        promise: () => Promise.resolve({
            Body: '{"Request": {"Status": "Successful",}}'
        }),
    });
    AWS.mock('S3', 'getObject', getObjectPromise);
    // executions
    let result = s3Handler.downloadFile({
        Bucket: "bucketName",
        Key: "key"
    });
    result.then(rta => {
        expect(rta).toBeDefined();
        expect(rta.Body).toEqual('{"Request": {"Status": "Successful",}}');
    })
    // assertions

})