const yaml = require('yaml');
let separator = '.';

let yamlLoader = (yamlFile) => {
    let doc = yaml.parse(yamlFile);
    let configuration = getAsProperties(doc);
    let separatorFiltered = configuration.filter(entry => entry[0] === 'separator');
    if (separatorFiltered.length > 0) {
        separator = separatorFiltered[0][1];
        console.log('A separator has bing detected',separator);
        let resultFiltered = configuration.filter(entry => entry[0] !== 'separator');
        return resultFiltered.map(entry => [entry[0].replace('.', separator), entry[1]])
    } else {
        return configuration;
    }
}



function getAsProperties(ob) {
    function getList(entry, prefix = '', list = []) {
        if (entry !== undefined) {
            Object.entries(entry).forEach(obj => {
                if (typeof obj[1] === 'object') {
                    const pfx = prefix + obj[0] + '.';
                    return getList(obj[1], pfx, list);
                } else {
                    const value = typeof obj[1] === 'string' ? obj[1].replace(/\n/g, '\\n') : obj[1]
                    list.push([prefix + obj[0], value]);
                    getList(undefined, '', list);
                }

            });
        }
        return list;
    }
    return getList(ob);
}

module.exports.yamlLoader = yamlLoader;