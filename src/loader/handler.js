'use strict';
const s3Object = require('../s3-handler/s3-handler');
const yamlParser = require('../yml-parser/yml-parser');
const ssmManager = require('../ssm-manager/ssm-manager');
const names = require('../utils/names')
module.exports.ssmLoader = (event, context, callback) => {
    event.Records.forEach(record => {
        const key = record.s3.object.key;
        const bucketName = record.s3.bucket.name;
        let configRequest = {};
        names.getNames(key, bucketName, (error, data) => {
            if(error){
                console.log(error);
                throw new Error(error);
            }else{
                configRequest = data;
            }
        })

        s3Object.downloadFile({
            Bucket: bucketName,
            Key: key
        }).then((result) => {
            console.log('We get the file');
            return yamlParser.yamlLoader(result.Body.toString('utf-8'));
        }).then((configuration) => ssmManager.loadConfiguration(configRequest, configuration))
            .catch((error) => {
                console.log('Something wrong happend', error);
                callback('Something wrong happend');
            })
        console.log(`bucket name ${bucketName}`);
        console.log(`New config file has been created: ${configRequest.fileName}`);


    });
}


