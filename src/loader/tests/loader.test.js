
const s3Object = require('../../s3-handler/s3-handler');
const ssmManager = require('../../ssm-manager/ssm-manager');
const loader = require('../handler');

jest.mock('../../s3-handler/s3-handler.js')
jest.mock('../../ssm-manager/ssm-manager.js')

test('it should load a configuration', () => {
    // setup 
    s3Object.downloadFile.mockResolvedValue({
            Body: Buffer.from( "hello: value", 'utf8')
        });
    ssmManager.loadConfiguration.mockImplementation((value, configuration) => {
        expect(value).toMatchObject({
            cloudName: "bb",
            projectName: "cc",
            environment: "dev",
            service: "component"
        })
        expect(configuration).toEqual(expect.arrayContaining([ [ 'hello', 'value' ] ]))
    })

    
    let event = {
        Records: [{
            s3: {

                object: {
                    key: "component/dev/config.yml"
                },
                bucket: {
                    name: "bb-cc-config-server"
                }
            }
        }]
    };

    // executions
     loader.ssmLoader(event, undefined, (error) => {
        console.log(error);
    })

    // assertions
    expect(s3Object.downloadFile).toHaveBeenCalledTimes(1);
    

})
