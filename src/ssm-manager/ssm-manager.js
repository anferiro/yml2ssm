let AWS = require('aws-sdk');


let loadConfiguration = ({ cloudName, projectName, environment, service }, configuration) => {
    let ssm = new AWS.SSM();

    Object.entries(configuration).forEach(conf => {
        console.log(`Key ${conf[1][0]} - value ${conf[1][1]}`);
        let parameter = {
            Name: `/${cloudName}-${environment}/${projectName}/${service}/${conf[1][0]}`,
            Type: "String",
            Value: conf[1][1],
            Overwrite: true
        }
        ssm.putParameter(parameter, (error, data) => {
            if (error) {
                console.log(error);
            } else {
                console.log(parameter, data);
            }
        });
    })
}

let deleteConfiguration = ({ cloudName, projectName, environment, service }, callback) => {
    let ssm = new AWS.SSM();
    const request = {
        Path: `/${cloudName}-${environment}/${projectName}/${service}/`,
        Recursive: false
    }
    ssm.getParametersByPath(request, (error, data) => {
        let ssm = new AWS.SSM();
        if (error) {
            console.log(error);
            callback(error, undefined); 
        } else {
            let listParameters = [];
            console.log('Data:',data);
            data.Parameters.forEach(parameter => {
                console.log('Parameter', parameter);
                console.log('Parameter to delete ',parameter.Name);
                listParameters.push(parameter.Name);

            });
            
            ssm.deleteParameters({Names: listParameters}, (error,data) => {
                if (error) {
                    console.log(error);
                    callback(error, undefined);
                }else{
                    console.log('The data eliminated ',data)
                    callback(undefined,data);
                }
            });
        }
    });
}

module.exports = {
    loadConfiguration,
    deleteConfiguration
};