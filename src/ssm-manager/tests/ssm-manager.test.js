var AWS = require('aws-sdk-mock');
const ssmManager = require('../ssm-manager');
const sinon = require('sinon');

test('it should call twice the putParameter function', () => {
    // setup 
    let parameterSpy = sinon.spy();
    AWS.mock('SSM', 'putParameter', parameterSpy);
    let configuration = {
        cloudName: "bb",
        projectName: "cc",
        environment: "dev",
        service: "component",
        fileName: "config.yml"
    }
    // executions
    ssmManager.loadConfiguration(configuration, [["p1.s1", "v1"], ["p2.s2", "v2"]]);

    // assertions
    expect(parameterSpy.getCalls().length).toEqual(2);
    expect(parameterSpy.getCall(0).args[0]).toMatchObject({
        Name: '/bb-dev/cc/component/p1.s1',
        Type: "String",
        Value: "v1",
        Overwrite: true
    });
    expect(parameterSpy.getCall(1).args[0]).toMatchObject({
        Name: '/bb-dev/cc/component/p2.s2',
        Type: "String",
        Value: "v2",
        Overwrite: true
    });
    AWS.restore('SSM', 'putParameter')

});

test('it should add two parameters', () => {
    // setup 
    let putParameterStub = sinon.stub();
    putParameterStub.callsFake((param, callback) => {
        callback(undefined, {
            Version: "1"
        })
    });

    AWS.mock('SSM', 'putParameter', putParameterStub);
    let configuration = {
        cloudName: "bb",
        projectName: "cc",
        environment: "dev",
        service: "component",
        fileName: "config.yml"
    }
    // executions
    ssmManager.loadConfiguration(configuration, [["p1.s1", "v1"], ["p2.s2", "v2"]]);

    // assertions
    expect(putParameterStub.called).toEqual(true);
    expect(putParameterStub.getCalls().length).toEqual(2);
    AWS.restore('SSM', 'putParameter')

});

test('it should return an error calling putParameter', () => {
    // setup 
    let putParameterSutp = sinon.stub();
    putParameterSutp.callsFake((param, callback) => {
        callback("Somthing went wrong", undefined)
    });

    AWS.mock('SSM', 'putParameter', putParameterSutp);
    let configuration = {
        cloudName: "bb",
        projectName: "cc",
        environment: "dev",
        service: "component",
        fileName: "config.yml"
    }
    // executions
    ssmManager.loadConfiguration(configuration, [["p1.s1", "v1"], ["p2.s2", "v2"]]);

    // assertions
    expect(putParameterSutp.called).toEqual(true);
    expect(putParameterSutp.getCalls().length).toEqual(2);
    AWS.restore('SSM', 'putParameter')

});

test('it should delete two parameters', () => {
    // setup 
    let getParametersStub = sinon.stub();
    getParametersStub.callsFake((param, callback) => {
        callback(undefined, {
            Parameters: [
                {
                    Name: "p1.s1",
                    Value: "v1"
                },
                {
                    Name: "p2.s2",
                    Value: "v2"
                }]
        });
    });
    let deleteParameterStub = sinon.stub();
    deleteParameterStub.callsFake((param, callback) => {
        callback(undefined, "ok");
    });
    AWS.mock('SSM', 'getParametersByPath', getParametersStub);
    AWS.mock('SSM', 'deleteParameters', deleteParameterStub);
    let configuration = {
        cloudName: "bb",
        projectName: "cc",
        environment: "dev",
        service: "component",
        fileName: "config.yml"
    }
    // executions
    ssmManager.deleteConfiguration(configuration, (error, data) => {
        if (error) {
            console.log(error);
        } else {
            console.log(data);
            expect(data).toBeDefined();
            expect(data).toHaveLength(2);
        }
    });

    // assertions
    expect(getParametersStub.getCalls().length).toEqual(1);
    expect(deleteParameterStub.getCalls().length).toEqual(1);

    AWS.restore('SSM', 'getParametersByPath')
    AWS.restore('SSM', 'deleteParameters')

});

test('it should return error gettingParameters', () => {
    // setup 
    let getParametersStub = sinon.stub();
    getParametersStub.callsFake((param, callback) => {
        callback("Error getting the data",undefined);
    });
    
    AWS.mock('SSM', 'getParametersByPath', getParametersStub);
    let configuration = {
        cloudName: "bb",
        projectName: "cc",
        environment: "dev",
        service: "component",
        fileName: "config.yml"
    }
    // executions
    ssmManager.deleteConfiguration(configuration, (error, data) => {
        if (error) {
            console.log(error);
            expect(error).toBeDefined();
        } else {
            console.log(data);
        }
    });

    // assertions
    expect(getParametersStub.getCalls().length).toEqual(1);
    
    AWS.restore('SSM', 'getParametersByPath')
    
});

test('it should return error deleting parameters', () => {
    // setup 
    let getParametersStub = sinon.stub();
    getParametersStub.callsFake((param, callback) => {
        callback(undefined, {
            Parameters: [
                {
                    Name: "p1.s1",
                    Value: "v1"
                },
                {
                    Name: "p2.s2",
                    Value: "v2"
                }]
        });
    });
    let deleteParameterStub = sinon.stub();
    deleteParameterStub.callsFake((param, callback) => {
        callback("Error deleting parameters",undefined);
    });
    AWS.mock('SSM', 'getParametersByPath', getParametersStub);
    AWS.mock('SSM', 'deleteParameters', deleteParameterStub);
    let configuration = {
        cloudName: "bb",
        projectName: "cc",
        environment: "dev",
        service: "component",
        fileName: "config.yml"
    }
    // executions
    ssmManager.deleteConfiguration(configuration, (error, data) => {
        if (error) {
            console.log(error);
            expect(error).toBeDefined();
        } else {
            console.log(data);
        }
    });

    // assertions
    expect(getParametersStub.getCalls().length).toEqual(1);
    expect(deleteParameterStub.getCalls().length).toEqual(1);

    AWS.restore('SSM', 'getParametersByPath')
    AWS.restore('SSM', 'deleteParameters')

});

